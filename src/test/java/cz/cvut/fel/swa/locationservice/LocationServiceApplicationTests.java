package cz.cvut.fel.swa.locationservice;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
class LocationServiceApplicationTests {
    @Autowired
    RestTemplate restTemplate;

    @Autowired
    private Controller controller;

    @Value("${api.url}")
    private String geocodingApiUrl;

    private MockRestServiceServer mockServer;

    @BeforeEach
    public void setUp() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
    }

    @Test
    void healthCheckTest() {
        assertThat(controller.healthCheck().getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void getCoordsFromCityName_correctCityName_correctResponse() {
        String cityName = "Prague";
        String url = UriComponentsBuilder.fromHttpUrl(geocodingApiUrl)
                .queryParam("q", cityName)
                .toUriString();

        String jsonResponse = "[\n" +
                "    {\n" +
                "        \"place_id\": 286715451,\n" +
                "        \"licence\": \"Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright\",\n" +
                "        \"powered_by\": \"Map Maker: https://maps.co\",\n" +
                "        \"osm_type\": \"relation\",\n" +
                "        \"osm_id\": 435514,\n" +
                "        \"boundingbox\": [\n" +
                "            \"49.9419006\",\n" +
                "            \"50.1774301\",\n" +
                "            \"14.2244355\",\n" +
                "            \"14.7067867\"\n" +
                "        ],\n" +
                "        \"lat\": \"50.0874654\",\n" +
                "        \"lon\": \"14.4212535\",\n" +
                "        \"display_name\": \"Prague, Czechia\",\n" +
                "        \"class\": \"boundary\",\n" +
                "        \"type\": \"administrative\",\n" +
                "        \"importance\": 0.8534914632787466\n" +
                "    }\n" +
                "]";

        mockServer.expect(requestTo(url))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withStatus(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(jsonResponse));

        var response = controller.getCoordsFromCityName(cityName);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(Objects.requireNonNull(response.getBody()).getCoordinates().getLatitude()).isEqualTo(50.0874654);
        assertThat(Objects.requireNonNull(response.getBody()).getCoordinates().getLongitude()).isEqualTo(14.4212535);
    }

    @Test
    void getCoordsFromCityName_incorrectCityName_notFoundResponse() throws Exception {
        String cityName = "Dudlakov";
        String url = UriComponentsBuilder.fromHttpUrl(geocodingApiUrl)
                .queryParam("q", cityName)
                .toUriString();

        mockServer.expect(requestTo(url))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withStatus(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body("[]"));

        var response = controller.getCoordsFromCityName(cityName);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }
}
