package cz.cvut.fel.swa.locationservice;

import cz.cvut.fel.swa.locationservice.data.Coordinates;
import cz.cvut.fel.swa.locationservice.data.CoordinatesResponse;
import cz.cvut.fel.swa.locationservice.data.LocationData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@org.springframework.stereotype.Service
public class Service {
    @Value("${api.url}")
    private String BASE_URL;

    @Autowired
    private RestTemplate restTemplate;

    private static final Logger logger = LoggerFactory.getLogger(Controller.class);

    public ResponseEntity<CoordinatesResponse> getCoordsFromCityName(@RequestParam String cityName) {
        String url = UriComponentsBuilder.fromHttpUrl(BASE_URL)
                .queryParam("q", cityName)
                .toUriString();

        LocationData[] locationDataArray = restTemplate.getForObject(url, LocationData[].class);

        if (locationDataArray == null) {
            return new ResponseEntity<>(new CoordinatesResponse("The geocoding API seems to be out of service."), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (locationDataArray.length == 0) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        LocationData locationData = locationDataArray[0];

        Coordinates coordinates = new Coordinates(locationData.getLat(), locationData.getLon());

        logger.info("Responding with coordinates " + coordinates + " to the city \"" + cityName + "\"");

        return new ResponseEntity<>(new CoordinatesResponse(coordinates), HttpStatus.OK);
    }
}
