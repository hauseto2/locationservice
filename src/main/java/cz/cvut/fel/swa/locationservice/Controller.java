package cz.cvut.fel.swa.locationservice;

import cz.cvut.fel.swa.locationservice.data.CoordinatesResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    @Autowired
    private Service service;

    @GetMapping("/")
    public ResponseEntity<String> healthCheck() {
        return new ResponseEntity<>("Location service is OK.", HttpStatus.OK);
    }

    @GetMapping("/city-name-to-coords")
    public ResponseEntity<CoordinatesResponse> getCoordsFromCityName(@RequestParam String cityName) {
        return service.getCoordsFromCityName(cityName);
    }
}
