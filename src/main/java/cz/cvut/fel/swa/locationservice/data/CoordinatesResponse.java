package cz.cvut.fel.swa.locationservice.data;

public class CoordinatesResponse {
    private final Coordinates coordinates;
    private final String errorMessage;

    public CoordinatesResponse(Coordinates coordinates) {
        this.errorMessage = "";
        this.coordinates = coordinates;
    }

    public CoordinatesResponse(String errorMessage) {
        this.errorMessage = errorMessage;
        this.coordinates = new Coordinates(0, 0);
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
