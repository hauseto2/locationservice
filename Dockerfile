ARG SERVICE_PORT=8084
FROM openjdk:17-oracle
COPY target/*.jar location-service.jar
EXPOSE $SERVICE_PORT
CMD ["java", "-jar", "location-service.jar"]
HEALTHCHECK	--interval=10s --timeout=3s --start-period=15s \
			CMD wget --quiet --tries=1 --spider http://localhost:$SERVICE_PORT || exit 1