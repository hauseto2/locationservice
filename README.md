# Weather App Location Service
This service is responsible for retrieving latitude and longitude given a city name from an external API.
## Setup
To run the SWA Prediction Service, build the Docker image and run the container by executing the following commands:
   ```bash
   mvn clean install 
   docker build -t=YOUR_CONT_NAME .
   docker run YOUR_CONT_NAME
   ```
## About
Once the service is up and running, you can access the API documentation to explore the available endpoints and their
functionalities. The Open API Swagger documentation can be accessed at http://localhost:8084/swagger-ui/index.html)
## Authors
- Tomáš Hauser - hauseto2@fel.cvut.cz
- Vadym Ostapovych - ostapva@fel.cvut.cz


